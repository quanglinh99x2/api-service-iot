<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Max30110Controller;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Api\AirDataController;
use App\Http\Controllers\Api\AlertController;
use App\Http\Controllers\Api\ScheduleController;
use App\Http\Controllers\Api\Wificontroller;





/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
 file định tuyến các api, định nghĩa ra các url của api
 cấu trúc câu lệnh:
 Route::phương_thức('url',[controller_xử_lý::class,'hàm_xử_lý_trong_controller_đó']);
|
*/


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'middleware' => ['api', 'auth.jwt'],
    'prefix' => 'auth'

], function ($router) {
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/refresh', [AuthController::class, 'refresh']);
    Route::get('/user-profile', [AuthController::class, 'userProfile']);
    Route::post('/change-pass', [AuthController::class, 'changePassWord']);
});


Route::group([
    'middleware' => ['api', 'auth.jwt'],
    'prefix' => 'user'
], function ($router) {
    Route::get('/list-user-by-manager',  [UserController::class, 'getListUserByIdManager']);
    Route::get('/get-all-user/{type}',  [UserController::class, 'getAllUser']);
    Route::post('/update-relation-user', [UserController::class, 'updateRoleUser']);
    Route::post('/add-relation-user', [UserController::class, 'addRoleUser']);
    Route::post('/delete-relation-user', [UserController::class, 'deleteRoleUser']);
    Route::post('/update-info/{user_id}', [UserController::class, 'updateInfoUser']);
  
    
    // Route::get('/list-user-from-building-manager/{buildingManagerId}',  [UserController::class, 'getListPartientFromBuildingManagerId']);
});

Route::group([
    'prefix' => 'airdata'
], function ($router) {
    Route::get('get-list-by-user/{user_id}',  [AirDataController::class, 'index'])->middleware('api', 'auth.jwt');
    Route::get('create',  [AirDataController::class, 'store']);
    Route::get('update/{id}',  [AirDataController::class, 'update']);
    Route::delete('delete/{id}',  [AirDataController::class, 'destroy']);
});

Route::group([
    'prefix' => 'max30110'
], function ($router) {
    Route::get('get-list-by-user/{user_id}',  [Max30110Controller::class, 'index'])->middleware('api', 'auth.jwt');
    Route::get('get-max-by-user-load/{user_id}',  [Max30110Controller::class, 'loadMax'])->middleware('api', 'auth.jwt');
    Route::get('get-list-by-user/week/{user_id}',  [Max30110Controller::class, 'getForWeek'])->middleware('api', 'auth.jwt');
    Route::get('create', [Max30110Controller::class, 'store']);
    Route::get('update/{id}', [Max30110Controller::class, 'update']);
    Route::delete('delete/{id}', [Max30110Controller::class, 'destroy']);
    Route::get('data-limit-device/{device_code}/{channel}',  [Max30110Controller::class, 'getDataLimit']);
    Route::get('get-max30110-by-time', [Max30110Controller::class, 'getMaxByDatetime']);
});
Route::get('create-data-max',  [Max30110Controller::class, 'autoCreateDataMax30110']);
Route::group([
    'middleware' => ['api', 'auth.jwt'],
    'prefix' => 'admin'
], function ($router) {
    // tạo lịch nhắc
    Route::group([
        'prefix' => 'schedule'
    ], function ($router) {
        Route::post('create',  [AlertController::class, 'store']);
        Route::put('update/{id}',  [ScheduleController::class, 'update']);
        Route::delete('delete/{id}',  [ScheduleController::class, 'destroy']);

        Route::get("get-list-schedule-by-patient/{patient_id}", [AlertController::class, 'getListScheduleByPatient']);
        Route::get('get-all-schedule-cron', [AlertController::class, 'getAllScheduleCron'])->middleware('cors');
    });
    // CRUD wifi
    Route::group([
        'prefix' => 'wifi'
    ], function ($router) {
        Route::post('create',  [Wificontroller::class, 'store']);//định tuyến api tạo mới wifi 
        Route::put('update/{id}',  [Wificontroller::class, 'update']);//định tuyến api cập nhật wifi
        Route::delete('delete/{id}',  [Wificontroller::class, 'destroy']);// định tuyết api xóa wifi
        Route::get("get-wifi/{id_device}", [Wificontroller::class, 'getWifiByIdDeviceForApp']);//định tuyến api lấy wifi cho web và app
        Route::get('get-all-for-device/{id_device}',[Wificontroller::class, 'getWifiByIdDeviceForDevice'])
        ->middleware('cors');//định tuyến các api lấy wifi active cho thiết bị
    });
});

//lấy lịch nhắc
Route::group([
    'prefix' => 'alert',
    'middleware' => ['cors']
], function () {
    Route::get('get-danger-devive',[AlertController::class, 'getAlertDangerForDevice']);
    Route::get('get-all-alert-cron', [AlertController::class, 'getAllAlertCron']);
    Route::get('create-alert-emergency',  [AlertController::class, 'createAlertEmergency']); //tạo cảnh báo
    Route::get('get-alert-app/{user_id}',  [AlertController::class, 'getAlertScheduleApp']); //lấy cảnh báo cho app
    Route::get('get-alert-device',  [AlertController::class, 'getAlertScheduleDevice']); // lấy cảnh báo cho thiết bị
    Route::get('update-status-schedule',[AlertController::class,'updateAlertSchedule']);//thay doi trang thai cua lich nhac
});

Route::group([
    'prefix' => 'device'
], function ($router) {
    Route::get('get-channel-by-device-code/{device_code}',  [UserController::class, 'getChannelByDeviceCode']);
    //Lấy các thiết bị bác sỹ or người nhà đó quản lý
    Route::get('get-device-by-user',  [UserController::class, 'getDeviceByUser']);

});

Route::get('fake-max3010',  [Max30110Controller::class, 'fakeChiSoSucKhoe']);
// Route::get('fake-max3010',  [Max30110Controller::class, 'fakeChiSoSucKhoe']);
