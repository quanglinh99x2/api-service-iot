<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnUserIdToMax30110Days extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('max30110_days', function (Blueprint $table) {
            //
            $table->integer('user_id');
            $table->integer('hour');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('max30110_days', function (Blueprint $table) {
            //
            $table->dropColumn('user_id');
            $table->dropColumn('hour');
        });
    }
}
