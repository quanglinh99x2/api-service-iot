/*
 Navicat Premium Data Transfer

 Source Server         : mysql_local
 Source Server Type    : MySQL
 Source Server Version : 100421
 Source Host           : localhost:3306
 Source Schema         : iot

 Target Server Type    : MySQL
 Target Server Version : 100421
 File Encoding         : 65001

 Date: 15/11/2021 22:10:45
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for airdata
-- ----------------------------
DROP TABLE IF EXISTS `airdata`;
CREATE TABLE `airdata`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `co` double(8, 2) NOT NULL,
  `gas` double(8, 2) NOT NULL,
  `status` int NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of airdata
-- ----------------------------
INSERT INTO `airdata` VALUES (1, 9, 1000.00, 1000.00, 1, '2021-10-28 16:34:49', '2021-10-28 18:02:26');
INSERT INTO `airdata` VALUES (3, 9, 1000.00, 1000.00, 1, '2021-10-28 18:01:16', '2021-10-28 18:01:16');

-- ----------------------------
-- Table structure for alert
-- ----------------------------
DROP TABLE IF EXISTS `alert`;
CREATE TABLE `alert`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `type_alert` int NOT NULL,
  `status` int NOT NULL,
  `process` int NOT NULL,
  `schedule_id` int NOT NULL,
  `max30100_id` int NOT NULL,
  `air_data_id` int NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of alert
-- ----------------------------

-- ----------------------------
-- Table structure for buildings
-- ----------------------------
DROP TABLE IF EXISTS `buildings`;
CREATE TABLE `buildings`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of buildings
-- ----------------------------

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `failed_jobs_uuid_unique`(`uuid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for max30110
-- ----------------------------
DROP TABLE IF EXISTS `max30110`;
CREATE TABLE `max30110`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `ir` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `bpm` double(8, 2) NOT NULL,
  `spo2` double(8, 2) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of max30110
-- ----------------------------
INSERT INTO `max30110` VALUES (1, 9, '100', 100.00, 100.00, '2021-10-23 10:38:47', '2021-10-31 14:41:05');
INSERT INTO `max30110` VALUES (2, 2, '22', 22.00, 22.00, '2021-10-23 10:38:47', '2021-10-23 10:38:49');
INSERT INTO `max30110` VALUES (3, 3, '33', 22.00, 22.00, '2021-10-23 10:38:47', '2021-10-23 10:38:49');
INSERT INTO `max30110` VALUES (4, 1, '44', 22.00, 22.00, '2021-10-23 10:38:47', '2021-10-23 10:38:49');
INSERT INTO `max30110` VALUES (5, 1, '55', 22.00, 22.00, '2021-10-23 10:38:47', '2021-10-23 10:38:49');
INSERT INTO `max30110` VALUES (6, 1, '66', 22.00, 22.00, '2021-10-23 10:38:47', '2021-10-23 10:38:49');
INSERT INTO `max30110` VALUES (7, 9, '100', 100.00, 100.00, '2021-10-31 14:39:06', '2021-10-31 14:39:06');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (4, '2019_12_14_000001_create_personal_access_tokens_table', 1);
INSERT INTO `migrations` VALUES (5, '2021_10_22_044404_create_max30110s_table', 1);
INSERT INTO `migrations` VALUES (6, '2021_10_22_044558_create_schedules_table', 1);
INSERT INTO `migrations` VALUES (7, '2021_10_22_044906_create_relationships_table', 1);
INSERT INTO `migrations` VALUES (8, '2021_10_22_045436_create_airdatas_table', 1);
INSERT INTO `migrations` VALUES (9, '2021_10_22_045617_create_alerts_table', 1);
INSERT INTO `migrations` VALUES (10, '2021_10_29_030304_create_buildings_table', 2);
INSERT INTO `migrations` VALUES (11, '2021_10_29_032640_add_column_building_id_to_users', 2);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for personal_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `last_used_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `personal_access_tokens_token_unique`(`token`) USING BTREE,
  INDEX `personal_access_tokens_tokenable_type_tokenable_id_index`(`tokenable_type`, `tokenable_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of personal_access_tokens
-- ----------------------------

-- ----------------------------
-- Table structure for relationship
-- ----------------------------
DROP TABLE IF EXISTS `relationship`;
CREATE TABLE `relationship`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int NULL DEFAULT NULL,
  `doctor_id` int NULL DEFAULT NULL,
  `manager_id` int NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of relationship
-- ----------------------------
INSERT INTO `relationship` VALUES (1, 9, 4, NULL, '2021-10-28 05:14:34', '2021-10-28 05:14:34');
INSERT INTO `relationship` VALUES (2, 9, 5, NULL, '2021-10-28 05:14:34', '2021-10-28 05:14:34');
INSERT INTO `relationship` VALUES (3, 9, 6, NULL, '2021-10-28 05:14:34', '2021-10-28 05:14:34');
INSERT INTO `relationship` VALUES (4, 1, NULL, 1, '2021-10-28 05:26:57', '2021-10-28 05:26:57');
INSERT INTO `relationship` VALUES (5, 3, NULL, 3, '2021-10-28 05:26:57', '2021-10-28 05:26:57');

-- ----------------------------
-- Table structure for schedule
-- ----------------------------
DROP TABLE IF EXISTS `schedule`;
CREATE TABLE `schedule`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `time` datetime(0) NOT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `process` int NOT NULL,
  `user_id` int NOT NULL,
  `create_by_id` int NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of schedule
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `fullname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` int NOT NULL,
  `role` int NOT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `job` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `building_id` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_username_unique`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'linhdq', '$2y$10$fFNQsiiyuanJ0qwWtVOOE.sV0Rpdtslm6dGcehLaRWflAkbhv9xIu', 'quanglinhit19@gmail.com', 'Dong Quang Linh', '0983763823', 1, 1, 'HN', NULL, NULL, NULL, NULL, '2021-10-22 08:09:53', '2021-10-22 08:09:53', 0);
INSERT INTO `users` VALUES (2, 'root', '$2y$10$gImBLgFerEDcke6L9rktfOkIXOaI8dkXu59keoI9BuLaPCSe1nOaO', 'root@gmail.com', 'Root User', '0983763823', 1, 4, 'HN', NULL, NULL, NULL, NULL, '2021-10-28 01:35:55', '2021-10-28 01:35:55', 0);
INSERT INTO `users` VALUES (3, 'abc', '$2y$10$CUhnbPNwmCsEeHSSOKuf6uYlniARnQezKY9V16y2Eo3PCrVZmHvYO', 'root12@gmail.com', 'Root User', '0983763823', 1, 1, 'HN', NULL, NULL, NULL, NULL, '2021-10-28 02:50:38', '2021-10-28 02:50:38', 0);
INSERT INTO `users` VALUES (4, 'bacsi1', '$2y$10$G0ogwfGmTyinOPIlcpxs0OorhbTO851YLZgU4uQ5rfQ.LADMyYMcy', 'bacsi1@gmail.com', 'Root User', '0983763823', 33, 2, 'HN', NULL, NULL, NULL, NULL, '2021-10-28 03:13:55', '2021-10-28 03:13:55', 0);
INSERT INTO `users` VALUES (5, 'bacsi2', '$2y$10$GeC6dpQc2ioL7DM8PBEGdemtI56fI72EbSX2.GLPJYq/L/5U2d3b2', 'bacsi2@gmail.com', 'bacsi2', '0983763823', 33, 2, 'HN', NULL, NULL, NULL, NULL, '2021-10-28 03:14:29', '2021-10-28 03:14:29', 0);
INSERT INTO `users` VALUES (6, 'bacsi3', '$2y$10$7oJzCmITmL7l3BRMvrXV9OebcfnH71dQFagyionMcvIhFrJ9Jcnd6', 'bacsi23@gmail.com', 'bacsi3', '0983763823', 33, 2, 'HN', NULL, NULL, NULL, NULL, '2021-10-28 03:14:46', '2021-10-28 03:14:46', 0);
INSERT INTO `users` VALUES (9, 'benhnhan4', '$2y$10$doMEcWDV3xpfSFtKXhcQNuyuAwxIAwmUV8XkdvXe9aC/A9yLSQU7y', 'benhnahn4@gmail.com', 'Bệnh nhân 4', '039372362', 12, 1, 'HN', NULL, NULL, NULL, NULL, '2021-10-28 05:14:34', '2021-10-28 05:14:34', 0);
INSERT INTO `users` VALUES (13, 'nguoinha1', '$2y$10$Mw7MOfmUSOCopsFG03rEE.5hRa/DuuxSnCD5iOubRwRMkW/9ujZy2', 'ngươinha1@gmail.com', 'Người nhà 1', '039372362', 13, 3, 'HN', NULL, NULL, NULL, NULL, '2021-10-28 05:26:57', '2021-10-28 05:26:57', 0);

SET FOREIGN_KEY_CHECKS = 1;
