<?php

namespace App\Traits;

use Exception;

trait BaseResponse
{
    public function getResponse($code, $message, $data) {
        return response()->json(
                [
                    'code' => $code,
                    'message' => $message,
                    'data' => $data
                ], 200
            );
    }
     public function getResponseDevice($data) {
         try{
            return response()->json(
                [
                    "id"=>$data->id,
                    "code"=>$data->message_code,
                    "message"=>$data->content
                   
                ]
                , 200
                );
         }catch(Exception $ex){
            return response()->json(
                [
                    "id"=>0,
                    "message"=>"",
                    "message_code"=>""
                ]
                , 200
                );
         }
        
    }

    public function converDataForDevice($data) {
        try{
            if(is_null($data->value1)){
                return response()->json(
                    [
                        "value1"=>""
                    ]
                    , 200
                    );
            }else{
                return response()->json(
                    $data
                    , 200
                    );
            }
        }catch(Exception $ex){
           return response()->json(
               [
                   "value1"=>""
               ]
               , 200
               );
        }
       
   }
    public function getError($code,$message,$httpCode)
    {
        return response()->json(
            [
                'code' => $code,
                'message' => $message,
            ], $httpCode
        );
    }
}