<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Alert;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Carbon;


class ResetScheduleCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alert:reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'reset alert is loop';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $alerts=Alert::where("is_loop",1)->get();
        foreach($alerts as $item){
            $item->status=1;
            $item->process=1;
            $time=Carbon::parse($item->time);
            $time=$time->addDay();
            $item->time=$time->format("Y-m-d H:i");
            $item->save();
        }
    }
}
