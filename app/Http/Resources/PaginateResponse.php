<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PaginateResponse extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'pageIndex' => $this->currentPage(),
            'pageSize' => $this->perPage(),
            'totalItems' => $this->total(),
            'totalPages' => $this->lastPage(),
            'data' => $this->items()
        ];
    }
}
