<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class AlertCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $result=array();
        foreach($this->collection as $data) {
            $result[]= [
                'id' => $data->id,
                'type_alert' => $data->type_alert,
                'status' => $data->status,
                'process' => $data->process,
                'message' => $data->content,
                'message_code'=>$data->message_code
            ];
        }
        return $result;
    }
}
