<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AlertResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $result = [
            'pageIndex' => $this->currentPage(),
            'pageSize' => $this->perPage(),
            'totalItems' => $this->total(),
            'totalPages' => $this->lastPage(),
            'data'=>array()
        ];
        foreach($this->items() as $item){
            $imp=[
                "id"=>$item->id,
                "type_alert"=>$item->type_alert,
                "status"=>$item->status,
                "process"=>$item->process,
                "content"=>$item->content,
                "time"=>$item->time,
                "create_by"=>$item->user_create->fullname,
                "is_loop"=>$item->is_loop
            ];
            $result["data"][]=$imp;
        }
       
        return $result;
    }
}
