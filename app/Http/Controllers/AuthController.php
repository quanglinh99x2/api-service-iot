<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Validator as FacadesValidator;
use App\Traits\BaseResponse;
use App\Models\Relationship;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
class AuthController extends Controller
{
    use BaseResponse;
    private $user;
    private $relationship;
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct(User $user,Relationship $relationship) {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
        $this->user = $user;
        $this->$relationship = $relationship;
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request){
    	$validator = FacadesValidator::make($request->all(), [
            // 'email' => 'required|email',
            // 'username' => 'required',
            // 'password' => 'required|string|min:6',
        ]);
        if($request->username == null || empty($request->username)){
            return $this->getResponse("01", "Username is blank", false);
        }
        if($request->password == null || empty($request->password)){
            return $this->getResponse("02", "Password is blank", false);
        }
        if(strlen($request->password) < 6){
            return $this->getResponse("03", "Password at least 6 characters", false);
        }
        // if ($validator->fails()) {
        //     return response()->json($validator->errors(), 422);
        // }
        $credentials = [
            'username' =>$request->username,
            'password' => $request->password,
        ];

        if (! $token = auth('api')->attempt($credentials)) {
            return $this->getResponse("04", "Unauthorized", false);
            // return response()->json(['error' => 'Unauthorized'], 401);
        }
        return $this->createNewToken($token);
    }

    /**
     * Register a User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request) {
        // $validator = FacadesValidator::make($request->all(), [
        //     'username' => 'required|string|between:2,100',
        //     'email' => 'required|string|email|max:100|unique:users',
        //     'password' => 'required|string|min:6',
        // ]);
        if($request->username == null || empty($request->username)){
            return $this->getResponse("01", "Username is blank", false);
        }
        if($request->password == null || empty($request->password)){
            return $this->getResponse("02", "Password is blank", false);
        }
        if($request->email == null || empty($request->email)){
            return $this->getResponse("03", "Email is blank", false);
        }
        if(strlen($request->password) < 6){
            return $this->getResponse("04", "Password at least 6 characters", false);
        }

        // if( auth('api')->user() == null ){
        //     return $this->getResponse("05", "You are not logged in", false);
        // }
        // if( auth('api')->user() != null){
        //     if(auth('api')->user()->role != 4 ){
        //         return $this->getResponse("06", "Not a user root", false);
        //     }
        // }
        $userRegis = $this->user->where('username', $request->username)->first();
        if($userRegis != null){
            return $this->getResponse("07", "Username existed", false);
        }
        // if($validator->fails()){
        //     return $this->getResponse("08", $validator->errors(), false);
        // }
        $checkUserDevice = $this->user->where('device_code',$request->deviceCode)
        ->where('channel', $request->channel)->first();
        if($checkUserDevice != null){
            return $this->getResponse("11", "Thiết bị: ".$request->deviceCode." đã tồn tại với kênh: ".$request->channel."", false);
        }
        try{
            DB::beginTransaction();
            $user = new User();
            $user->fullname = $request->fullname;
            $user->username = $request->username;
            $user->password = bcrypt($request->password);
            $user->email = $request->email;
            $user->age = $request->age;
            $user->note = $request->note;
            $user->age = $request->age;
            $user->sex = $request->sex;
            $user->role = $request->role;
            $user->health_insurance = $request->healthInsurance;
            $user->phone = $request->phone;
            $user->address = $request->address;
            $user->building_id = $request->buildingId;
            $user->device_code = $request->deviceCode;
            $user->channel = $request->channel;
            $user->ir_limit = $request->ir_limit;
            $user->bpm_limit = 0.70 * (200 - $request->age) / 2.3;
            $user->bpm_limit_max = 0.80 * (200 - $request->age) / 2.3;
            $user->save();

            // Nếu là bệnh nhân -> chọn danh sách bác sỹ
            Log::error('lst doctor: '.$request->lstDoctorId);
            if ($request->role == 1) {
                if(!empty($request->lstDoctorId) && $request->lstDoctorId != '[]'){
                    foreach ($request->lstDoctorId as $doctorId) {
                        $doctor = $this->user->find($doctorId);
                        if( $doctor == null){
                            DB::rollBack();
                            return $this->getResponse("08", "Doctor dose not exist with id :$doctorId", false);
                        }
                        if($doctor->role != 2){
                            DB::rollBack();
                            return $this->getResponse("08", "Doctor dose not exist with id :$doctorId", false);
                        }
                        $relationship = new Relationship();
                        $relationship->user_id = $user->id;
                        $relationship->doctor_id = $doctorId;
                        $relationship->save();
                    }
                }
                if(!empty($request->lstManagerId  && $request->lstDoctorId != '[]')){
                    foreach ($request->lstManagerId as $managerId) {
                        $manager = $this->user->find($managerId);
                        if( $manager == null){
                            DB::rollBack();
                            return $this->getResponse("09", "Manager dose not exist with id :$managerId", false);
                        }
                        if($doctor->role != 2){
                            DB::rollBack();
                            return $this->getResponse("09", "Manager dose not exist with id :$managerId", false);
                        }
                        $relationship = new Relationship();
                        $relationship->user_id = $user->id;
                        $relationship->manager_id = $managerId;
                        $relationship->save();
                    }
                }
            }
            // Thêm mới người nhà
            if($request->role == 3){
                if(!empty($request->lstUserId  && $request->lstDoctorId != '[]')){
                    foreach ($request->lstUserId as $userId) {
                        $user = $this->user->find($userId);
                        if( $user == null){
                            DB::rollBack();
                            return $this->getResponse("10", "Patient dose not exist with id :$userId", false);
                        }
                        if($user->role != 1){
                            DB::rollBack();
                            return $this->getResponse("10", "Patient dose not exist with id :$userId", false);
                        }
                        $relationship = new Relationship();
                        $relationship->user_id = $userId;
                        $relationship->manager_id = $user->id;
                        $relationship->save();
                    }
                }
            }

            DB::commit();
            return $this->getResponse("00", "User successfully registered", $user);
        }
        catch(\Exception $exception){
            Log::error('Message :'.$exception->getMessage().' - Line:'.$exception->getLine());
            DB::rollBack();
            return $this->getResponse("99", "Has exception.", $exception);
        }
    }


    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout() {
        Auth::logout();
        return $this->getResponse("00", "Logout successfully", true);
        // return response()->json(['message' => 'User successfully signed out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh() {
        return $this->createNewToken(Auth::refresh());
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile() {

        return response()->json( $this->getResponse("00", "Success", auth('api')->user()));
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token){
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
            'user' => auth('api')->user()
        ]);
    }

    public function changePassWord(Request $request) {
        $validator = FacadesValidator::make($request->all(), [
            'new_password' => 'required|string|min:6',
            'confirm_password' => 'required|string|same:new_password|min:6'
        ]);
        $userId = auth()->user()->id;
        if($validator->fails()){
            return $this->getResponse("02", $validator->errors(), false);
            // return response()->json($validator->errors()->toJson(), 400);
        }

        $user = User::where('id', $userId)->update(
                    ['password' => bcrypt($request->new_password)]
                );
                return $this->getResponse("00", "User successfully changed password", User::find($userId));
        // return response()->json([
        //     'message' => 'User successfully changed password',
        //     'user' => $user,
        // ], 201);
    }
}
