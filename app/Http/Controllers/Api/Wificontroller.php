<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Traits\BaseResponse;
use Illuminate\Pagination\Paginator;
use App\Models\Wifi;
use Exception;
use App\Http\Resources\PaginateResponse;
use Illuminate\Support\Facades\Log;
use \stdClass;


class Wificontroller extends Controller
{
    use BaseResponse;
    private $wifi;

    public function __construct(Wifi $wifi)
    {
        $this->wifi=$wifi;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{
            DB::beginTransaction();
            $wf=new wifi();
            $wf->name=$request->name;
            $wf->pass=$request->pass;
            $wf->id_device=$request->id_device;
            $wf->status=1;//1:active,2:inactive
            $wf->save();
            DB::commit();
            return $this->getResponse("00", "Success", null);
        }catch(Exception $e)
        {
            DB::rollBack();
            Log::error($ex);
            return $this->getError("99","Internal Server Error",500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try{
            DB::beginTransaction();
            $wf=$this->wifi->find($id);
            if(is_null($wf)){
                return $this->getResponse("01", "Record not exists", null);
            }
            $wf->name=$request->name;
            $wf->pass=$request->pass;
            $wf->id_device=$request->id_device;
            $wf->status=$request->status;//1:active,2:inactive
            $wf->save();
            DB::commit();
            return $this->getResponse("00", "Success", null);
        }catch(Exception $e)
        {
            DB::rollBack();
            Log::error($e);
            return $this->getError("99","Internal Server Error",500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try{
            DB::beginTransaction();
            $this->wifi->destroy($id);
            DB::commit();
            return $this->getResponse("00", "Success", null);
        }catch(Exception $e)
        {
            DB::rollBack();
            Log::error($ex);
            return $this->getError("99","Internal Server Error",500);
        }
    }
    public function getWifiByIdDeviceForApp($id_device,Request $request)
    {
        try{
            $pageIndex = $request->header('pageIndex');
            $pageSize = $request->header('pageSize');
            Paginator::currentPageResolver(function () use ($pageIndex) {
                return $pageIndex;
            });
            $wf = DB::table('wifi')
            ->where('id_device',$id_device)->paginate($pageSize);;
            return $this->getResponse("00", "Success",  new PaginateResponse($wf));
        } catch (Exception $ex) {
            Log::error($ex);
            return $this->getError("99", "Internal Server Error", 500);
        }
      
    }

    public function getWifiByIdDeviceForDevice($id_device)
    {
        try{
            $wf = DB::table('wifi')
            ->where('id_device',$id_device)->where('status',1)->get();
                $result = array();
                $i=1;
           foreach ($wf as $data) {
                $result[] = [
                    'value'.$i => $data->name."|".$data->pass
                ];
                $i++;
           }
           $object = new stdClass();
           foreach ($result as $value)
           {
            foreach ($value as $key2 => $value2)
            {
               $object->$key2 = $value2;
            }
           }
            return $this->converDataForDevice($object);
        } catch (Exception $ex) {
            Log::error($ex);
            return $this->getError("99", "Internal Server Error", 500);
        }
    }
}
