<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Max30110;
use App\Models\User;
//===============lib_response================//
use Illuminate\Pagination\Paginator;
use App\Traits\BaseResponse;
use App\Traits\Utils;
use App\Http\Resources\PaginateResponse;
use App\Models\Max30110Day;
use Carbon\Carbon;
//===========================================//
use Exception;
use App\Models\Alert;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

define("HEALTH_DANGER",7);
define("BPM_DANGER",2);
class Max30110Controller extends Controller
{
    //
    use BaseResponse;
    use Utils;
    private $max30110;
    private $user;
    private $alert;
    private $max30110Day;

    public function __construct(Max30110 $max30110,User $user,Alert $alert,Max30110Day $maxDay)
    {
        $this->middleware('auth:api',['except' => [
            'addLimitTableMax30110',
            'fakeTongHopChiSoSucKhoeTheoNgay',
            'fakeChiSoSucKhoe',
            'store',
            'autoCreateDataMax30110',
            'getDataLimit',
            'addRealTime',
            'getMaxByDatetime'
            ]]);
        $this->max30110 = $max30110;
        $this->user=$user;
        $this->alert = $alert;
        $this->max30110Day=$maxDay;
    }
   
    public function getDataLimit($device_code, $channel){
        try{
            $dataLimit = $this->user
            ->where('device_code', $device_code)
            ->where('channel', $channel)
            ->first();
            $result = [
                'ir_limit' => $dataLimit->ir_limit,
                'bpm_limit' => $dataLimit->bpm_limit,
                'bpm_limit_max' => $dataLimit->bpm_limit_max,
            ];
            return response()->json(
                $result
                , 200
            );
        }catch(\Exception $ex){
            Log::error($ex);
            return $this->getResponse("99", "Exception", $ex);
        }
    }

    public function getForWeek(Request $request,$user_id)
    {
        $time=$request->header("time");
        $day=Carbon::parse($time);
            $data=array();
            for($i=1;$i<=7;$i++){
                $max30110 = $this->max30110
                ->where('device_code',$request->header("deviceCode"))
                ->where('channel',$request->header('channel'))
                ->whereDate('created_at', '=', $day->format('Y-m-d'))->get();
                $avg_ir=0;
                $avg_bpm=0;
                $avg_spo2=0;
                $total=0;
                foreach($max30110 as $item){
                    $total++;
                    $avg_ir+=$item->ir;
                    $avg_bpm+=$item->bpm;
                    $avg_spo2+=$item->spo2;
                }
                if($total!=0){
                    $avg_ir=$avg_ir/$total;
                    $avg_bpm=$avg_bpm/$total;
                    $avg_spo2=$avg_spo2/$total;
                }
                $max =new Max30110();
                $max->user_id=$user_id;
                $max->created_date=$day->format('Y-m-d');
                $max->ir=$avg_ir;
                $max->bpm=$avg_bpm;
                $max->spo2=$avg_spo2;
                $data[]=$max;
                $day=$day->subDay();
            }
            return $this->getResponse("00", "Success",$data);
    }

    public function index(Request $request,$user_id)
    {
        try{
            
            // if (auth('api')->user()->role == 3 || auth('api')->user()->role == 2) {
                $pageIndex = $request->header('pageIndex');
                $pageSize = $request->header('pageSize');
                $time=$request->header("time");
                Paginator::currentPageResolver(function () use ($pageIndex) {
                    return $pageIndex;
                });
                $max30110 = $this->max30110->where('device_code',$request->header('deviceCode'))
                ->where('channel',$request->header('channel'))
                ->whereDate('created_at', '=', $time)->paginate($pageSize);
                // foreach($max30110 as $item){
                //     $item->created_at="vlc";
                // }
                return $this->getResponse("00", "Success", new PaginateResponse($max30110));
            // } else {
            //     return $this->getError(403,"Forbidden",403);
            // }
        } catch (Exception $ex) {
            Log::error($ex);
            return $this->getError("99","Internal Server Error",500);
        }
    }

    public function loadMax(Request $request,$user_id){
        $max30110 = $this->max30110->where('user_id', $user_id)
        ->orderBy('created_at', 'DESC')
        ->get();
        if(empty($max30110)){
            return $this->getResponse("01", "Không có dữ liệu cho user", null);
        }
        $lastMax = $max30110[0];
        // dd(Carbon::parse($lastMax->created_at)->format('Y-m-d'));
        $max30110OnDay = $this->max30110->where('user_id', $user_id)
        ->whereDate('created_at', Carbon::parse($lastMax->created_at)->format('Y-m-d'))
        ->get();
        return $this->getResponse("00", "Success",$max30110OnDay);
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $user = $this->user->where('device_code', $request->get("device_code"))
            ->where('channel', $request->get("channel"))->first();
            if($user->bpm_limit>$request->get('bpm')||$user->bpm_limit_max<$request->get('bpm'))
            {
                $message="Bệnh nhân ".$user->fullname." chỉ số nhịp tim bất thường!";
                $token=$this->generateRandomString(50);
                $this->saveAlertDangerHealthForDevice($user->device_code,$user,$message,$request,$token);
                $user_together_device_code=$this->user->where('device_code',$request->get('device_code'))->get();
                foreach($user_together_device_code as $item)
                {
                    if($item->id==$user->id) continue;
                    $this->saveAlertJHealthDanger($item->id,$user,$message,$request,$token);
                }
                foreach ($user->doctors as $doctor) {

                    $this->saveAlertJHealthDanger($doctor->id,$user,$message,$request,$token);
                }
                foreach ($user->managers as $manager) {
                    $this->saveAlertJHealthDanger($manager->id,$user,$message,$request,$token);
                }
            }
            $data = [
                'device_code'=>$request->get("device_code"),
                'channel' => $request->get("channel"),
                'user_id' => $user->id,
                'ir' => $request->get("ir"),
                'bpm'=> $request->get("bpm"),
                'spo2' =>$request->get("spo2"),
                'bpm_limit' => $user->bpm_limit,
                'bpm_limit_max' => $user->bpm_limit_max,
                'time' => Carbon::now()->format('Y-m-d h:i:s')
            ];
            $keyMax30110 = $user->id."_".Carbon::now()->hour;
            // dd($keyMax30110);
            $dataRedis = Redis::get($keyMax30110);
            if($dataRedis == null){
                $arr = array();
                $arr[] = $data;
                Redis::set($keyMax30110, json_encode($arr));
            }else{
                // dd($data);
                $data_json = json_decode($dataRedis);
                //Check bản ghi cuối cùng
                $endRecord = $data_json[sizeOf($data_json) - 1];
                //Nếu thời gian bản ghi cuối có phút trùng với phút hiện tại thì không thêm bản ghi vào nữa
                if(Carbon::parse($endRecord->time)->minute == Carbon::now()->minute){
                    return $this->getResponse("00", "Success", null);
                }
                $data_json[] = $data;
                Redis::set($keyMax30110, json_encode($data_json));
            }
            // $max30110->save();
            DB::commit();
            return $this->getResponse("00", "Success", null);
        } catch (Exception $ex) {
            DB::rollBack();
            Log::error($ex);
            return $this->getError("99","Internal Server Error",500);
        }
    }

    public function getMaxByDatetime(Request $request){
        try {
            $time=$request->time;
            $userId = $request->userId;
            //Generate key để lấy data từ cache về 
            //Nếu là ngày hiện tại sẽ lấy data từ Cache ra để trả về
            if (Carbon::now()->format('Y-m-d') == Carbon::parse($time)->format('Y-m-d')){
                $keyInCache = $userId."_".Carbon::parse($time)->hour;
                // dd(true);
                $dataRedis =  Redis::get($keyInCache);
                if($dataRedis == null){
                    Log::info("getMaxByDatetime data redis null");
                    return $this->getResponse("00", "Success", array());
                }
                return $this->getResponse("00", "Success", json_decode($dataRedis));
            } else {
                //Lấy dữ liệu theo ngày tạo ứng với thời gian, user id nhận được
                $max30110Day = Max30110Day::whereDate('created_at', Carbon::parse($time)->format('Y-m-d'))
                        ->where('user_id', $userId)
                        ->where('hour', Carbon::parse($time)->hour)
                        ->first();
                if($max30110Day != null){
                    return $this->getResponse("00", "Success", json_decode($max30110Day->data));
                }else{
                    return $this->getResponse("00", "Success", array());
                }
            }
        } catch(Exception $ex){
            Log::error($ex);
            return $this->getError("99","Has exception",200);
        }
    }

    public function autoCreateDataMax30110()
    {
        try{
            DB::beginTransaction();
        $user=$this->user->where("role",1)->get();
        foreach($user as $item){
            for($i=0;$i<=Carbon::now()->hour;$i++){
               if(isset($item->device_code)&&isset($item->channel)){
                $max30110 = new max30110();
                $max30110->user_id =$item->id;
                $max30110->ir =rand(20,100);
                $max30110->bpm = rand(20,100);
                $max30110->spo2 =rand(20,100);
                $max30110->device_code =$item->device_code;
                $max30110->channel =$item->channel;
                $max30110->created_at=Carbon::now()->subHour($i);
                $max30110->save();
               }
            }
        }
        DB::commit();
        return $this->getResponse("00", "Success", null);
    } catch (Exception $ex) {
            DB::rollBack();
            Log::error($ex);
            return $this->getError("99","Internal Server Error",500);
        }
      
    }
    public function update(Request $request, $id)
    {
        //
        try {
            DB::beginTransaction();
            $max30110 = $this->max30110->find($id);
            $max30110->user_id = $request->user_id;
            $max30110->ir = $request->ir;
            $max30110->bpm = $request->bpm;
            $max30110->spo2 = $request->spo2;
            $max30110->save();
            DB::commit();
            return $this->getResponse("00", "Success", null);
        } catch (Exception $ex) {
            DB::rollBack();
            Log::error($ex);
            return $this->getError("99","Internal Server Error",500);
        }
    }
    public function destroy($id)
    {
        //
        try {
            $max30110 = $this->max30110->find($id);
            $max30110->delete();
            return $this->getResponse("00", "Success", null);
        } catch (Exception $ex) {
            Log::error($ex);
            return $this->getError("99","Internal Server Error",500);
        }
    }

    public static function saveAlertJHealthDanger($send_to_user_id,$user_patient,$message,Request $request,$token)
    {
        $schedule = new Alert();
        $schedule->content = $message;
        $schedule->user_id = $user_patient->id;
        $schedule->type_alert=BPM_DANGER;
        $schedule->send_to_user = $send_to_user_id;
        $schedule->status = 1;
        $schedule->process = 1;
        $schedule->message_code=HEALTH_DANGER;
        $schedule->create_by_id =  $user_patient->id;
        $schedule->token=$token;
        $schedule->bpm=$request->get("bpm");
        $schedule->bpm_max=$user_patient->bpm_limit_max;
        $schedule->bpm_min=$user_patient->bpm_limit;
        $schedule->spo2=$request->get("spo2");
        $schedule->save();
    }

    public static function saveAlertDangerHealthForDevice($device_code,$user_patient,$message,Request $request,$token)
    {
        $alert = new Alert();
        $alert->user_id = $user_patient->id;
        $alert->content = $message;
        $alert->type_alert=BPM_DANGER;
        $alert->status = 1;
        $alert->process = 1;
        $alert->device_code=$device_code;
        $alert->create_by_id =  $user_patient->id;
        $alert->message_code=HEALTH_DANGER;
        $alert->token=$token;
        $alert->bpm=$request->get("bpm");
        $alert->bpm_max=$user_patient->bpm_limit_max;
        $alert->bpm_min=$user_patient->bpm_limit;
        $alert->spo2=$request->get("spo2");
        $alert->save();
    }

    public function fakeChiSoSucKhoe()
    {
         //$request->get('user_id');
         try {
             // benh nhan tran bao t
             //benh nhan nguyen van th
             //dong thi h
             //
            $device_code=3;
            $channel=2;
            $time_start_str="2022-01-28 06:00:00";
            $time_stop_str="2022-01-28 23:00:00";
            $time_start=Carbon::parse($time_start_str);
            $time_stop=Carbon::parse($time_stop_str);
           // DB::beginTransaction();
           for($k=0;$k<2;$k++){
            $user = $this->user->where('device_code', $device_code)
            ->where('channel', $channel)->first();
            while($time_start->format('Y-m-d H:i:s')!=$time_stop->format('Y-m-d H:i:s')){
            $data=array();
            $time_start_temp=$time_start;
            for($i=0;$i<=59;$i++){
            $data[] = [
                'device_code'=>$device_code,
                'channel' => $channel,
                'user_id' => $user->id,
                'ir' => rand(20,100),
                'bpm'=> rand($user->bpm_limit,$user->bpm_limit_max),
                'spo2' =>rand(95,100),
                'bpm_limit' => $user->bpm_limit,
                'bpm_limit_max' => $user->bpm_limit_max,
                'time' =>$time_start_temp->addMinute()->format('Y-m-d H:i:s')
            ];
        }
          $max30Day=new Max30110Day();
          $max30Day->data=json_encode($data);
          $max30Day->user_id=$user->id;
          $max30Day->hour=$time_start->hour-1;
          $max30Day->created_at=$time_start->format('Y-m-d H:i:s');
          $max30Day->save();
          //$time_start=$time_start->addHour();
        }
        $this->fakeTongHopChiSoSucKhoeTheoNgay($time_start->format('Y-m-d H:i:s'));
        $time_start=Carbon::parse($time_start_str);
        $time_stop=Carbon::parse($time_stop_str);
        $time_start=$time_start->addDay();
        $time_stop=$time_stop->addDay();
       
    }

       // DB::commit();
            return $this->getResponse("00", "Success", null);
        } catch (Exception $ex) {
          //  DB::rollBack();
            Log::error($ex);
            return $this->getError("99","Internal Server Error",500);
        }
    }
    
    public function fakeTongHopChiSoSucKhoeTheoNgay($str_day)
    {
        $day_str=$str_day;
        $day=Carbon::parse($day_str);
        $maxDay=$this->max30110Day->whereDate('created_at', '=', $day->format('Y-m-d'))->get();
        if($maxDay==null){return;}
        foreach($maxDay as $item){
            $object=json_decode($item->data);
            $ir_avg=0;
            $bpm_avg=0;
            $spo2_avg=0;
            for($i=0;$i<count($object);$i++){
                $ir_avg+=$object[$i]->ir;
                $bpm_avg+=$object[$i]->bpm;
                $spo2_avg+=$object[$i]->spo2;
            }

            $ir_avg=$ir_avg/count($object);
            $bpm_avg=$bpm_avg/count($object);
            $spo2_avg=$spo2_avg/count($object);

            $max30110 = new max30110();
            $max30110->user_id =$item->user_id;
            $max30110->ir =$ir_avg;
            $max30110->bpm = $bpm_avg;
            $max30110->spo2 = $spo2_avg;
            $max30110->device_code =$object[0]->device_code;
            $max30110->channel =$object[0]->channel;
            $max30110->created_at=$item->created_at;
            $max30110->save();
        }
    }

    public function addLimitTableMax30110()
    {
        $maxData=$this->max30110->where('bpm_limit',0)->get();
        foreach($maxData as $item){
            $user=$this->user->find($item->user_id);
            $item->bpm_limit=$user->bpm_limit;
            $item->bpm_limit_max=$user->bpm_limit_max;
            $item->save();
        }
    }
}
