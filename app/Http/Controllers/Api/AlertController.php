<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\AlertResource;
use Illuminate\Http\Request;
use App\Models\Alert;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use App\Traits\BaseResponse;
use App\Traits\Utils;
use Illuminate\Pagination\Paginator;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Carbon;
use \stdClass;

use function PHPUnit\Framework\isEmpty;

define("AlERT_SCHEDULE",3);
define("ALERT_DANGER",1);
define("CODE_ALERT_DANGER",07);
define("CODE_COMFIRM",8);
define("CONFIRM_TYPE",8);

class AlertController extends Controller
{
    use BaseResponse;
    use Utils;
    private $alert;
    private $user;
    public function __construct(Alert $alert, User $user)
    {
        $this->middleware('auth:api',[
            'except' => [
                'getAllAlertCron',
                'getAllScheduleCron',
                'updateAlertSchedule',
                'getAlertScheduleDevice',
                "createAlertEmergency",
                "getAlertDangerForDevice"
                ]]);
        $this->alert = $alert;
        $this->user = $user;
    }

    public function updateAlertSchedule(Request $request)
    {
        try{
            DB::beginTransaction();
            $alert =$this->alert->find($request->get('id'));
            $alert->status=2;
            $alert->save();

        //tạo alert cho xác nhận cho thiết bị con
        $user_type=0;
        if(!is_null($alert->send_to_user)){
            $user_confirm=$this->user->find($alert->send_to_user);
            $user_type=$user_confirm->role;
        }else{
            $user_type=1;
        }
            $user_patient=$this->user->find($alert->user_id);
            $message="";
            $this->saveAlertConfirmForDevice($user_patient->device_code,$user_patient->id,$user_type,$message);
            DB::commit();
            return $this->getError("00", "Success", 200);

        }catch(Exception $ex){
            DB::rollBack();
            Log::error($ex);
            return $this->getError("99", "Internal Server Error", 500);
        }
    }

    public function createAlertEmergency(Request $request)
    {
        try {
                DB::beginTransaction();
                $user_patient = $this->user->where('device_code',$request->get('device_code'))
                ->where('channel',$request->get('channel'))->first();

                $message="Bệnh nhân ".$user_patient->fullname." đang cần hỗ trợ gấp!";
                $token=$this->generateRandomString(49);
                $this->saveAlertDangerForDevice($user_patient->device_code,$user_patient,$message,$token);

                $user_together_device_code=$this->user->where('device_code',$request->get('device_code'))->get();

                foreach($user_together_device_code as $item)
                {
                    if($item->id==$user_patient->id) continue;
                    $this->saveAlertDanger($item->id,$user_patient,$message,$token);
                }

                foreach ($user_patient->doctors as $doctor) {

                    $this->saveAlertDanger($doctor->id,$user_patient,$message,$token);
                }

                foreach ($user_patient->managers as $manager) {
                    $this->saveAlertDanger($manager->id,$user_patient,$message,$token);
                }

                DB::commit();
                return $this->getResponse("00", "Success", null);
            }
         catch (Exception $ex) {
            DB::rollBack();
            Log::error($ex);
            return $this->getError("99", "Internal Server Error", 500);
        }

    }
    public static function saveAlertConfirmForDevice($device_code,$id_user_patient,$user_type,$message)
    {
        $alert = new Alert();
        $alert->user_id = $id_user_patient;
        $alert->content = $message;
        $alert->type_alert=CONFIRM_TYPE;
        $alert->status = 1;
        $alert->process = 1;
        $alert->device_code=$device_code;
        $alert->send_to_user = $id_user_patient;
        $alert->create_by_id =  $user_type; //1: thiết bị mẹ 2: người nhà, 3 bác sĩ
        $alert->message_code=CODE_COMFIRM;
        $alert->save();
    }
    public static function saveAlertDangerForDevice($device_code,$user_patient,$message,$token)
    {
        $alert = new Alert();
        $alert->user_id = $user_patient->id;
        $alert->content = $message;
        $alert->type_alert=ALERT_DANGER;
        $alert->status = 1;
        $alert->process = 1;
        $alert->device_code=$device_code;
        $alert->create_by_id =  $user_patient->id;
        $alert->message_code=CODE_ALERT_DANGER;
        $alert->token=$token;
        $alert->save();
    }
    public static function saveAlertDanger($send_to_user_id,$user_patient,$message,$token)
    {
        $schedule = new Alert();
        $schedule->content = $message;
        $schedule->user_id = $user_patient->id;
        $schedule->type_alert=ALERT_DANGER;
        $schedule->send_to_user = $send_to_user_id;
        $schedule->status = 1;
        $schedule->process = 1;
        $schedule->message_code=CODE_ALERT_DANGER;
        $schedule->create_by_id =  $user_patient->id;
        $schedule->token=$token;
        $schedule->save();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAlertScheduleDevice(Request $request)
    {
        //
        try {
            DB::beginTransaction();
            if($request->get('channel')==null){
                $alert = DB::table('alert')
                ->where("device_code", $request->get('device_code'))
                ->where('status', 1)
                ->where('process',1)
                ->where('time', Carbon::now()->addMinutes(1)->format("Y-m-d H:i"))
                ->first();
                if(isset($alert->id)){
                        $temp = $this->alert->find($alert->id);
                        $temp->process = 2;
                        $temp->save();
                }
                if(isset($alert->message_code)){
                    $user=$this->user->find($alert->user_id);
                    $alert->message_code= $alert->message_code."_".$user->sex."|".$user->channel;
                    }
            }else{
            $user = $this->user->where('device_code',$request->get('device_code'))->where('channel',$request->get('channel'))->first();
            if(is_null($user)){
                return $this->getResponse("01", "User not exists", null);
            }
            $alert = DB::table('alert')
                ->where("send_to_user", $user->id)
                ->where('status', 1)
                ->where('process',1)
                ->where('time', Carbon::now()->addMinutes(1)->format("Y-m-d H:i"))
                ->first();
                if(isset($alert->id)){
                        $temp = $this->alert->find($alert->id);
                        $temp->process = 2;
                        $temp->save();
                }
               
            $alert_fail= DB::table('alert')
            ->where("send_to_user", $user->id)
            ->where('status', 1)
            ->where('time', Carbon::now()->subMinutes(5)->format("Y-m-d H:i"))
            ->get();
        
            foreach($alert_fail as $item){
                $user_patient = $this->user->find($item->send_to_user);
                $message="Bệnh nhân ".$user_patient->fullname." chưa hoàn thành ".$item->content." vào lúc ".$item->time;
                foreach ($user_patient->doctors as $doctor) {

                    $this->saveAlertDanger($doctor->id,$user_patient,$message);
                }
                foreach ($user_patient->managers as $manager) {
                    $this->saveAlertDanger($manager->id,$user_patient,$message);
                }
                $item->status=3;
                $item->save();
            }
            if(isset($alert->message_code)){
                $alert->message_code= $alert->message_code."_".$user->sex."|".$user->channel;
                }
        }
            DB::commit();
           

            return $this->getResponseDevice($alert);

        } catch (Exception $ex) {
            DB::rollBack();
            Log::error($ex);
            return $this->getError("99", "Internal Server Error", 500);
        }
    }
    public function getAllAlertCron()
    {
        try{
            $alert_list = DB::table('alert')
            ->whereNull('time')
            ->where('status', 1)
            ->get();
            $result = array();
            foreach($alert_list as $data){
                $result[] = [
                    'id' => $data->id,
                    'message' => $data->content,
                    'send_to_user'=>$data->send_to_user
                ];
                $alert=$this->alert->find($data->id);
                $alert->status=2;
                $alert->save();
            }
            return $this->getResponse("00", "Success", $result);
        } catch (Exception $ex) {
            Log::error($ex);
            return $this->getError("99", "Internal Server Error", 500);
        }
    }
    public function getAllScheduleCron()
    {
        try{
            $alert_list = DB::table('alert')
            ->where('time', Carbon::now()->addMinutes(1)->format("Y-m-d H:i"))
            ->where('status', 1)
            ->get();
            $result = array();
            foreach($alert_list as $data){
                $result[] = [
                    'id' => $data->id,
                    'message' => $data->content,
                    'send_to_user'=>$data->send_to_user
                ];
            }
            return $this->getResponse("00", "Success", $result);
        } catch (Exception $ex) {
            Log::error($ex);
            return $this->getError("99", "Internal Server Error", 500);
        }

    }
    public function getAlertScheduleApp($id)
    {
        try {
            if (auth('api')->user()->role == 2 || auth('api')->user()->role == 3) {
            $alert = DB::table('alert')
                ->where("send_to_user", $id)
                ->where('status', 1)
                ->get();
            $result = array();
            foreach ($alert as $data) {
                if (!isEmpty($data->time)) {
                    if ($data->time != Carbon::now()->addMinutes(1)->format("Y-m-d H:i")) {
                        continue;
                    }
                }
                $temp = $this->alert->find($data->id);
                $temp->status = 2;
                $temp->save();
                $result[] = [
                    'id' => $data->id,
                    'message' => $data->content
                ];
            }
            return $this->getResponse("00", "Success", $result);
        } else {
            return $this->getError("403", "Forbidden", 403);
        }
        } catch (Exception $ex) {
            Log::error($ex);
            return $this->getError("99", "Internal Server Error", 500);
        }
    }
    private function saveSchedule(Request $request, $send_to_user_id)
    {
        $schedule = new Alert();
        $schedule->time = Carbon::parse($request->time)->format("Y-m-d H:i");
        $schedule->content = $request->content;
        $schedule->is_loop=$request->is_loop;
        $schedule->process = 1;
        $schedule->user_id = $request->user_id;
        $schedule->message_code=$request->message_code;
        $schedule->type_alert=AlERT_SCHEDULE;
        $schedule->send_to_user = $send_to_user_id;
        $schedule->status = 1;
        $schedule->create_by_id = auth('api')->user()->id;
        $schedule->save();
    }

    private function saveScheduleForDevice(Request $request,$user)
    {
        $schedule = new Alert();
        $schedule->time = Carbon::parse($request->time)->format("Y-m-d H:i");
        $schedule->is_loop=$request->is_loop;
        $schedule->content = $request->content;
        $schedule->process = 1;
        $schedule->user_id = $request->user_id;
        $schedule->message_code=$request->message_code;
        $schedule->type_alert=AlERT_SCHEDULE;
        $schedule->status = 1;
        $schedule->create_by_id = auth('api')->user()->id;
        $schedule->device_code=$user->device_code;
        $schedule->save();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            if (auth('api')->user()->role == 2 || auth('api')->user()->role == 3) {
                DB::beginTransaction();

                if($this->checkDuplicateTimeShedule($request)==false){
                    return $this->getError("452", "Thời gian lịch nhắc đã được sử dụng!", 452);
                };

                $user_patient = $this->user->find($request->user_id);
                $this->saveScheduleForDevice($request,$user_patient);
                $this->saveSchedule($request, $request->user_id);

                foreach ($user_patient->doctors as $doctor) {
                    $this->saveSchedule($request, $doctor->id);
                }

                foreach ($user_patient->managers as $manager) {
                    $this->saveSchedule($request, $manager->id);
                }

                DB::commit();
                return $this->getResponse("00","Success", null);
            } else {
                return $this->getError("403", "Forbidden", 403);
            }
        } catch (Exception $ex) {
            DB::rollBack();
            Log::error($ex);
            return $this->getError("99", "Internal Server Error", 500);
        }
    }

    private function checkDuplicateTimeShedule(Request $request)
    {
        $schedule=$this->alert->where('time',Carbon::parse($request->time)->format("Y-m-d H:i"))
                              ->where('user_id',$request->user_id)
                              ->where('status',1)
                              ->get();
        return empty($schedule[0]);
    }

    public function getListScheduleByPatient($patient_id,Request $request)
    {
        try{

                $pageIndex = $request->header('pageIndex');
                $pageSize = $request->header('pageSize');
                Paginator::currentPageResolver(function () use ($pageIndex) {
                    return $pageIndex;
                });
            $alert=$this->alert->where('type_alert',AlERT_SCHEDULE)->where("send_to_user",$patient_id)->paginate( $pageSize);
            return $this->getResponse("00", "Success", new AlertResource($alert));
        } catch (Exception $ex) {
            Log::error($ex);
            return $this->getError("99", "Internal Server Error", 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {
            if (auth('api')->user()->role == 2 || auth('api')->user()->role == 3) {
                DB::beginTransaction();
                $schedule = $this->alert->find($id);
                if (auth('api')->user()->id != $schedule->create_by_id) {
                    return $this->getError("403", "Forbidden", 403);
                }
                if ($schedule->status == 2) {
                    return $this->getError("418", "Lịch đã hoàn thành không thể cập nhật", 418);
                }
                $schedule->time = Carbon::parse($request->time)->format("Y-m-d H:i");
                $schedule->content = $request->content;
                $schedule->process = $request->process;
                $schedule->save();
                DB::commit();
                return $this->getResponse("00", "Success", null);
            } else {
                return $this->getError("403", "Forbidden", 403);
            }
        } catch (Exception $ex) {
            DB::rollBack();
            Log::error($ex);
            return $this->getError("99", "Internal Server Error", 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            if (auth('api')->user()->role == 2 || auth('api')->user()->role == 3) {
                $schedule = $this->schedule->find($id);
                if (auth('api')->user()->id != $schedule->create_by_id) {
                    return $this->getError("403", "Forbidden", 403);
                }
                $schedule->delete();
                return $this->getResponse("00", "Success", null);
            } else {
                return $this->getError("403", "Forbidden", 403);
            }
        } catch (Exception $ex) {
            Log::error($ex);
            return $this->getError("99", "Internal Server Error", 500);
        }
    }

    public function getAlertDangerForDevice(Request $request)
    {
        try {
            DB::beginTransaction();
            $object = new stdClass();
            $alerts=null;
            if($request->get('channel')==null){
                $alerts = DB::table('alert')
                ->where("device_code", $request->get('device_code'))
                ->where('status', 1)
                ->where('process',1)
                ->where('type_alert','<>',AlERT_SCHEDULE)
                ->get();
            }else{
            $user = $this->user->where('device_code',$request->get('device_code'))->where('channel',$request->get('channel'))->first();
            if(is_null($user)){
                return $this->getResponse("01", "User not exists", null);
            }
            $alerts = DB::table('alert')
                ->where("send_to_user", $user->id)
                ->where('status', 1)
                ->where('type_alert','<>',AlERT_SCHEDULE)
                ->where('process',1)
                ->get();
        }
        $result = array();
        $i=1;
        foreach($alerts as $item){
            $temp = $this->alert->find($item->id);
            $temp->process = 2;
            $temp->save();
            $user_temp=$this->user->find($item->user_id);
            if($item->message_code==CODE_COMFIRM){
                $result[] = [
                    'value'.$i =>$item->message_code."_".$item->create_by_id
                ];
            }else{
                $result[] = [
                    'value'.$i => $item->id."=".$item->message_code."_".$user_temp->sex."*".$item->type_alert."|".$item->content
                ];
            }
            $i++;
        }
            foreach ($result as $value)
            {
                foreach ($value as $key2 => $value2)
                {
                $object->$key2 = $value2;
                }
            }
            DB::commit();
            return $this->converDataForDevice($object);
        } catch (Exception $ex) {
            DB::rollBack();
            Log::error($ex);
            return $this->getError("99", "Internal Server Error", 500);
        }
    }
}