<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Validator as FacadesValidator;
use App\Models\Relationship;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

//===============lib_response================//
use Illuminate\Pagination\Paginator;
use App\Traits\BaseResponse;
use App\Http\Resources\PaginateResponse;

//===========================================//
class UserController extends Controller
{
    use BaseResponse;

    private $user;
    private $relationship;

    //
    public function __construct(User $user, Relationship $relationship)
    {
        $this->middleware('auth:api');
        $this->user = $user;
        $this->relationship = $relationship;
    }

    public function getChannelByDeviceCode($device_code)
    {
        $result = $this->user->where('device_code', $device_code)->get();
        return $this->getResponse("00", "Success", $result);
    }

    //Lất
    public function getListUserByIdManager(Request $request)
    {

        $id = auth('api')->user()->id;
        $manager = $this->user->find($id);
        if ($manager == null) {
            return $this->getResponse("01", "Manager dose not exist with id: $manager->id", false);
        }
        $arr_resp = array();

        if ($manager->role == 2) {
            $lstResult = $manager->patients;
        }
        if ($manager->role == 3) {
            $lstResult = $manager->patientOfManager;
        }
        if ($manager->role == 5) {
            $buildingId = $manager->building_id;
            $lstResult = $this->user->where('role', 1)->where('building_id', $buildingId)->get();
        }
        if ($manager->role == 1) {
            $infoUser = [
                'id' => $manager->id,
                'fullname' => $manager->fullname,
                'username' => $manager->username,
                'age' => $manager->age,
                'role' => $manager->role,
                'phone' => $manager->phone,
                'address' => $manager->address,
                'note' => $manager->note,
                'health_insurance' => $manager->health_insurance,
                'device_code' => $manager->device_code,
                'channel' => $manager->channel,
                'ir_limit' => $manager->ir_limit,
                'bpm_limit' => $manager->bpm_limit,
                'bpm_limit_max' => $manager->bpm_limit_max,
                'created_at' => $manager->created_at,
                'lstDoctor' => $manager->doctors,
                'lstManager' => $manager->managers
            ];
            return $this->getResponse("00", "Get info user success", $infoUser);
        }

        foreach ($lstResult as $item) {
            $arr_resp[] = [
                'id' => $item->id,
                'fullname' => $item->fullname,
                'username' => $item->username,
                'age' => $item->age,
                'role' => $item->role,
                'phone' => $item->phone,
                'note' => $item->note,
                'health_insurance' => $item->health_insurance,
                'device_code' => $item->device_code,
                'channel' => $item->channel,
                'ir_limit' => $item->ir_limit,
                'bpm_limit' => $item->bpm_limit,
                'bpm_limit_max' => $item->bpm_limit_max,
                'address' => $item->address,
                'created_at' => $item->created_at,
                'lstDoctor' => $item->doctors,
                'lstManager' => $item->managers,
            ];
        }

        return $this->getResponse("00", "Get list patient success", $arr_resp);
    }

    /** Cập nhật user
     * @param Request $request
     * @return JsonResponse
     */
    public function updateInfoUser(Request $request, $userId)
    {
        try {
            DB::beginTransaction();
            // Lấy user cần cập nhật từ UserId
            $user = $this->user->find($userId);

            //Kiểm tra xem user đó có tồn tại ko
            if (!isset($user)) {
                // Nếu không tồn tại báo lỗi cho client
                return $this->getResponse("01", "User dose not exist", false);
            }
            // Khi bhyt có sự thay đổi, cần kiểm tra xem đã có user nào tồn tại mã bhyt này chưa
            if ($user->health_insurance != $request->healthInsurance) {
                $userCheck = $this->user->where('health_insurance', $request->healthInsurance)->first();
                if ($userCheck != null) {
                    return $this->getResponse("02", "Health insurance existed", false);
                }
            }
            $checkUserDevice = $this->user->where('device_code', $request->deviceCode)
                ->where('channel', $request->channel)->first();
            if ($checkUserDevice != null) {
                if(($user->bpm_limit == $request->bpm_limit) && ($user->bpm_limit_max == $request->bpm_limit_max)){
                    return $this->getResponse("03", "Thiết bị: " . $request->deviceCode . " đã tồn tại với kênh: " . $request->channel . "", false);
                }
            }
            $user->fullname = $request->fullname;
            $user->health_insurance = $request->healthInsurance;
            $user->phone = $request->phone;
            $user->address = $request->address;
            $user->ir_limit = $request->ir_limit;
            $user->bpm_limit = $request->bpm_limit;
            $user->bpm_limit_max = $request->bpm_limit_max;
            if ($request->age != $user->age) {
                $user->bpm_limit = 0.70 * (200 - $request->age) / 2.3;
                $user->bpm_limit_max = 0.80 * (200 - $request->age) / 2.3;
            }
            $user->age = $request->age;
            $user->sex = $request->sex;
            $user->note = $request->note;
            $user->device_code = $request->deviceCode;
            $user->channel = $request->channel;
            $user->save();
            DB::commit();
            return $this->getResponse("00", "Update user success", $user);
        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error("Has error: " . $ex);
            return $this->getResponse("99", "Internal Server Error", "");
        }
    }

    // public function getListPartientFromManagerId(Request $request){
    //     $manager = $this->user->find($request->managerId);
    //     if( $manager == null){
    //         return $this->getResponse("05", "Manager dose not exist with id: $request->doctorId", false);
    //     }
    //     if($manager->role != 3){
    //         return $this->getResponse("05", "Manager dose not exist with id: $request->doctorId", false);
    //     }
    //     return $this->getResponse("00", "Get list patient of manager success", );
    // }
    // public function getListPartientFromBuildingManagerId(Request $request){
    //     $buildingManager = $this->user->find($request->buildingManagerId);
    //     $buildingId = $buildingManager->building_id;
    //     if($buildingManager->role != 5){
    //         return $this->getResponse("01", "User not a building manager", false);
    //     }
    //     $users = $this->user->where('role',1)->where('building_id', $buildingId)->get();
    //     return $this->getResponse("00", "Get list patient of building manager success", $users);
    // }
    public function getAllUser(Request $request)
    {
        $currentUser = auth('api')->user();
        // if ($currentUser->role != 4) {
        //     return $this->getResponse("01", "User not a root", false);
        // }
        $pageIndex = $request->header('pageIndex');
        $pageSize = $request->header('pageSize');
        $type = $request->type;
        Paginator::currentPageResolver(function () use ($pageIndex) {
            return $pageIndex;
        });
        //type = 1 ->get all User
        if ($type == 1) {
            $users = $this->user->where('role', 1)->paginate($pageSize);
        }
        //type = 2 ->get all doctor
        if ($type == 2) {
            $users = $this->user->where('role', 2)->paginate($pageSize);
        }
        //type = 3 ->get all manager
        if ($type == 3) {
            $users = $this->user->where('role', 3)->paginate($pageSize);
        }
        // $paginate = new Paginator($max30110, $request->pageSize);

        return $this->getResponse("00", "Get all user success", new PaginateResponse($users));

    }

    public function updateRoleUser(Request $request)
    {
        $user = $this->user->find($request->userId);
        if ($user == null) {
            return $this->getResponse("01", "Not a user", false);
        }
        if ($user->role != 1) {
            return $this->getResponse("02", "Role user must be 1", false);
        }
        $currentUser = auth('api')->user();
        if ($currentUser->role != 2) {
            return $this->getResponse("03", "You not a doctor", false);
        }
        try {
            DB::beginTransaction();
            $this->relationship->where('user_id', $request->userId)->delete();
            if (!isset($request->lstDoctorId)) {
                return $this->getResponse("10", "lstDoctorId dose not null", false);
            }
            foreach ($request->lstDoctorId as $doctorId) {
                $doctor = $this->user->find($doctorId);
                if ($doctor == null) {
                    DB::rollBack();
                    return $this->getResponse("05", "Doctor dose not exist with id :$doctorId", false);
                }
                if ($doctor->role != 2) {
                    DB::rollBack();
                    return $this->getResponse("06", "Doctor dose not exist with id :$doctorId", false);
                }
                $relationship = new Relationship();
                $relationship->user_id = $user->id;
                $relationship->doctor_id = $doctorId;
                $relationship->save();
            }
            if (!isset($request->lstManagerId)) {
                return $this->getResponse("11", "lstManagerId dose not null", false);
            }
            foreach ($request->lstManagerId as $managerId) {
                $manager = $this->user->find($managerId);
                if ($manager == null) {
                    DB::rollBack();
                    return $this->getResponse("08", "Manager dose not exist with id :$managerId", false);
                }
                if ($doctor->role != 2) {
                    DB::rollBack();
                    return $this->getResponse("09", "Manager dose not exist with id :$managerId", false);
                }
                $relationship = new Relationship();
                $relationship->user_id = $user->id;
                $relationship->manager_id = $managerId;
                $relationship->save();
            }
            DB::commit();
            Log::info("Update success");
            return $this->getResponse("00", "Update user relationship successfully registered", $user);
        } catch (\Exception $exception) {
            Log::error('Message :' . $exception->getMessage() . ' - Line:' . $exception->getLine());
            DB::rollBack();
            return $this->getResponse("99", "System has error !", false);
        }
    }

    /**
     * Bác sỹ thêm mới bệnh nhân vào liên kết
     */
    public function addRoleUser(Request $request)
    {
        // dd($request->userId);
        // Lấy user đang đăng nhập hiện tại
        $currentUser = auth('api')->user();
        //Lấy user cần test ra để check quyền
        $userAdd = $this->user->find($request->userId);
        if ($userAdd == null) {
            return $this->getResponse("06", "User add does not exist", false);
        }
        if ($currentUser->role == 2 || $currentUser->role == 3) {
            foreach ($currentUser->patients as $user) {
                if ($user->id == $request->userId) {
                    return $this->getResponse("01", "Patient already exists in the relationship", false);
                }
            }
            if ($userAdd->role != 1) {
                return $this->getResponse("02", "User not a patient", false);
            }
        }
        if ($currentUser->role == 1) {
            foreach ($currentUser->doctors as $user) {
                if ($user->id == $request->userId) {
                    return $this->getResponse("03", "Doctor already exists in the relationship", false);
                }
            }
            foreach ($currentUser->managers as $user) {
                if ($user->id == $request->userId) {
                    return $this->getResponse("04", "Manager already exists in the relationship", false);
                }
            }
            if (!($userAdd->role == 2 || $userAdd->role == 3)) {
                return $this->getResponse("05", "User not a doctor or managers", false);
            }
        }
//        dd($userAdd);

        try {
            $relationship = new Relationship();
            if ($currentUser->role == 2 || $currentUser->role == 3) {
                $relationship->user_id = $request->userId;
                if ($currentUser->role == 2) {
                    $relationship->doctor_id = $currentUser->id;
                }
                if ($currentUser->role == 3) {
                    $relationship->manager_id = $currentUser->id;
                }
            }
            if ($currentUser->role == 1) {
                $relationship->user_id = $currentUser->id;
                if ($userAdd->role == 2) {
                    $relationship->doctor_id = $userAdd->id;
                }
                if ($userAdd->role == 3) {
                    $relationship->manager_id = $userAdd->id;
                }
            }
            $relationship->save();
            return $this->getResponse("00", "Success", $relationship);
        } catch (\Exception $ex) {
            Log::error('Message :' . $ex->getMessage() . ' - Line:' . $ex->getLine() . '-ex:' . $ex);
            DB::rollBack();
            return $this->getResponse("99", "System has error !", false);
        }
    }

    /**
     * Bác sỹ thêm mới bệnh nhân vào liên kết
     */
    public function deleteRoleUser(Request $request)
    {
        $currentUser = auth('api')->user();
        if (!($currentUser->role == 2 || $currentUser->role == 3)) {
            return $this->getResponse("01", "You not a doctor or manager", false);
        }
        $flag = false;
        foreach ($currentUser->patients as $user) {
            if ($user->id == $request->userId) {
                $flag = true;
                break;
            }
        }
        if (!$flag) {
            return $this->getResponse("02", "Patient dose not exists in the relationship", false);
        }
        $userAdd = $this->user->find($request->userId);
//        dd($userAdd);
        if ($userAdd == null) {
            return $this->getResponse("03", "Patient does not exist", false);
        }
        try {
            if ($currentUser->role == 2) {
                $relationship = $this->relationship
                    ->where('user_id', $request->userId)
                    ->where('doctor_id', $currentUser->id)
                    ->delete();
            }
            if ($currentUser->role == 3) {
                $relationship = $this->relationship
                    ->where('user_id', $request->userId)
                    ->where('manager_id', $currentUser->id)
                    ->delete();
            }
            return $this->getResponse("00", "Success", true);
        } catch (\Exception $ex) {
            Log::error('Message :' . $ex->getMessage() . ' - Line:' . $ex->getLine() . '-ex:' . $ex);
            DB::rollBack();
            return $this->getResponse("99", "System has error !", false);
        }
    }

    public function getDeviceByUser()
    {
        try {
            $currentUser = auth('api')->user();
            $arr = array();
            if ($currentUser->role == 1) {
                $user = $this->user->where('device_code', $currentUser->device_code)->get();
                $result = [
                    "device_code" => $currentUser->device_code,
                    "lstChannel" => $user
                ];
                $arr[] = $result;
                return $this->getResponse("00", "Success", $arr);
            } else if ($currentUser->role == 2) {
                $partial = $currentUser->patients;
                $arr = $this->getDataDeviceChannel($partial, $arr);
            } else if ($currentUser->role == 3) {
                $partial = $currentUser->patientOfManager;
                $arr = $this->getDataDeviceChannel($partial, $arr);
            }
            return $this->getResponse("00", "Success", $arr);
        } catch (\Exception $ex) {
            Log::error('Message :' . $ex->getMessage() . ' - Line:' . $ex->getLine() . '-ex:' . $ex);
            return $this->getResponse("99", "System has error !", false);
        }
    }

    /**
     * @param $partial
     * @param array $arr
     * @return array
     */
    public function getDataDeviceChannel($partial, array $arr)
    {
        $arrDeviceCode = array();
        foreach ($partial as $item) {
            if (!in_array($item->device_code, $arrDeviceCode)) {
                $arrDeviceCode[] = $item->device_code;
            }
        }
        foreach ($arrDeviceCode as $device) {
            $user = $this->user->where('device_code', $device)->get();
            $result = [
                "device_code" => $device,
                "lstChannel" => $user
            ];
            $arr[] = $result;
        }
        return $arr;
    }
}
