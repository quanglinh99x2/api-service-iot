<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Airdata;
use App\Models\Alert;
use App\Models\User;
use App\Traits\BaseResponse;
use App\Http\Resources\PaginateResponse;
use App\Http\Controllers\Api\AlertController;
use Exception;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


define("ALERT_DANGER_AIR",4);
define("CODE_AIR_DATA_DANGER",07);
class AirDataController extends Controller
{
    private $air_data;
    private $alert;
    private $user;
    use BaseResponse;
    public function __construct(Airdata $air_data,Alert $alert,User $user)
    {
        $this->middleware('auth:api',['except' => ['store']]);
        $this->user=$user;
        $this->air_data = $air_data;
        $this->alert=$alert;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,$user_id)
    {
        //
        try{
            // if(auth('api')->user()->role==3||auth('api')->user()->role==2){
                $pageIndex = $request->header('pageIndex');
                $pageSize = $request->header('pageSize');
                $time=$request->header('time');
                Paginator::currentPageResolver(function () use ($pageIndex) {
                    return $pageIndex;
                });
        
                $air_data = $this->air_data->where('user_id',$user_id)->whereDate('created_at', '=', $time)->paginate($pageSize);
        
                return $this->getResponse("00", "Success", new PaginateResponse($air_data));
            // }else{
            //     return $this->getError(403,"Forbidden",403);
            // }
        } catch (Exception $ex) {
            Log::error($ex);
            return $this->getError("99","Internal Server Error",500);
        }
      

    }

    public function store(Request $request)
    {
        //
        try {
            DB::beginTransaction();
           
            $user_patients = $this->user->where('device_code',$request->get("device_code"))->get();
            $message="Nhóm bệnh nhân ";
            foreach($user_patients as $patient){
                $message=$message. $patient->fullname.",";
            }
          $user_patient = $this->user->where('device_code',$request->get("device_code"))->first();
          $air_data=new airdata();
            $air_data->user_id= $user_patient->id;
            $air_data->co=$request->get('co',0);
            $air_data->gas=$request->get('gas',0);
            $air_data->status= $request->get('status',0);
            $air_data->device_code=$request->get("device_code");
            $air_data->save();
            $co_mess="";
            $gas_mess="";
            if($air_data->co>0){
                $co_mess="khí co vượt ngưỡng";
            }
            if($air_data->gas>0){
                $gas_mess="khí gas vượt ngưỡng";
            }
            $message=$message." đang trong môi trường ".$co_mess." , ".$gas_mess;
            $this->saveAlertAirDataDangerForDevice($request->get('device_code'),$user_patient,$message);
            foreach ($user_patient->doctors as $doctor) {
                
                $this->saveAlertDanger($doctor->id,$user_patient,$message);
            }
            foreach ($user_patient->managers as $manager) {
                $this->saveAlertDanger($manager->id,$user_patient,$message);
            }

            foreach($user_patients as $patient)
            {
                $this->saveAlertDanger($patient->id,$user_patient,$message);
            }

            DB::commit();
            return $this->getResponse("00", "Success", null);
        } catch (Exception $ex) {
            DB::rollBack();
            Log::error($ex);
            return $this->getError("99","Internal Server Error",500);
        }
    }

    public static function saveAlertDanger($send_to_user_id,$user_patient,$message)
    {
        $schedule = new Alert();
        $schedule->content = $message;
        $schedule->user_id = $user_patient->id;
        $schedule->type_alert=ALERT_DANGER_AIR;
        $schedule->send_to_user = $send_to_user_id;
        $schedule->status = 1;
        $schedule->process = 1;
        $schedule->message_code=CODE_AIR_DATA_DANGER;
        $schedule->create_by_id =  $user_patient->id;
        $schedule->save();
    }

    public static function saveAlertAirDataDangerForDevice($device_code,$user_patient,$message)
    {
        $alert = new Alert();
        $alert->content = $message;
        $alert->type_alert=ALERT_DANGER_AIR;
        $alert->status = 1;
        $alert->device_code=$device_code;
        $alert->create_by_id =$user_patient->id;
        $alert->message_code=CODE_AIR_DATA_DANGER;
        $alert->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try {
            DB::beginTransaction();
            $air_data=$this->air_data->find($id);
            $air_data->user_id= $request->get('user_id');
            $air_data->co=$request->get('co',0);
            $air_data->gas=$request->get('gas',0);
            $air_data->status= $request->get('status',0);
            $air_data->save();
            DB::commit();
            return $this->getResponse("00", "Success", null);
        } catch (Exception $ex) {
            DB::rollBack();
            Log::error($ex);
            return $this->getError("99","Internal Server Error",500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
           $air_data=$this->air_data->find($id);
            $air_data->delete();
            return $this->getResponse("00", "Success", null);
        } catch (Exception $ex) {
            Log::error($ex);
            return $this->getError("99","Internal Server Error",500);

        }
    }
}
