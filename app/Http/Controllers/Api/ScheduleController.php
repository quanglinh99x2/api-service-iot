<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Alert;
use App\Traits\BaseResponse;
use Carbon\Carbon;

use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ScheduleController extends Controller
{
    private $schedule;
    use BaseResponse;
    public function __construct(Alert $schedule) {
        $this->middleware('auth:api');
        $this->schedule=$schedule;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(auth('api')->user()->role==2||auth('api')->user()->role==3){
            try {
                dd($request->message_code);
                DB::beginTransaction();
                $schedule = new Alert();
                $schedule->time = $request->time;
                $schedule->message_code=$request->message_code;
                $schedule->content = $request->content;
                $schedule->process = $request->process;
                $schedule->user_id = $request->user_id;
                $schedule->create_by_id = auth('api')->user()->id;
                $schedule->save();
                DB::commit();
                return $this->getResponse("00", "Success", null);
            } catch (Exception $ex) {
                DB::rollBack();
                Log::error($ex);
                return $this->getResponse("99", "Internal Server Error", $ex);
            }
        }else{
            return $this->getResponse("101", "You don't have permission for this", null);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if(auth('api')->user()->role==2||auth('api')->user()->role==3){
        try {
            DB::beginTransaction();
            $schedule_old=$this->schedule->find($id);
            if(isset($schedule_old->time)){
                $schedule_old_list=$this->schedule->where("time",$schedule_old->time)->where("status",1)->get();
                foreach($schedule_old_list as $item){
                    $alert=$this->schedule->find($item->id);
                    $alert->time=$request->time;
                    $alert->message_code=$request->message_code;
                    $alert->content=$request->content;
                    $alert->save();
                }
            }
            // $schedule = new Alert();
            // $schedule->time = $request->time;
            // $schedule->content = $request->content;
            // $schedule->process = $request->process;
            // $schedule->save();
            DB::commit();
            return $this->getResponse("00", "Success", null);
        } catch (Exception $ex) {
            DB::rollBack();
            Log::error($ex);
            return $this->getResponse("99", "Internal Server Error", $ex);
        }
    }else{
        return $this->getResponse("101", "You don't have permission for this", null);
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if(auth('api')->user()->role==2||auth('api')->user()->role==3){
        try {
            DB::beginTransaction();
            $schedule=$this->schedule->find($id);
            $alert_all=$this->schedule->where("time",$schedule->time)->get();
            foreach($alert_all as $item){
                $item->delete();
            }
            DB::commit();
             return $this->getResponse("00", "Success", null);
         } catch (Exception $ex) {
             DB::rollBack();
             Log::error($ex);
             return $this->getResponse("99", "Internal Server Error", null);
         }
        }else{
            return $this->getResponse("101", "You don't have permission for this", null);
        }
    }
}
