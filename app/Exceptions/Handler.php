<?php

namespace App\Exceptions;

use ErrorException;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Response;
class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
            return  response()->json(
                [
                    'code'=>'96',
                    'message'=>'error'
                ],
                401);
        });
        $this->renderable(function(RouteNotFoundException $e, $request){
            return  response()->json(
                [
                    'code'=>'97',
                    'message'=>'Forbidden'
                ],
                403);
        });
        $this->renderable(function(ModelNotFoundException $e, $request){
            return  response()->json(
                [
                    'code'=>'98',
                    'message'=>'Resource not found'
                ],
                404);
        });
        $this->renderable(function(ErrorException $e, $request){
            return  response()->json(
                [
                    'code'=>'95',
                    'message'=>'Error exception'
                ],
               500);
        });
        // $this->renderable(function(Exception $e, $request){
        //     return  response()->json(
        //         [
        //             'code'=>'99',
        //             'message'=>'Error exists'
        //         ],
        //         404);
        // });
        $this->renderable(function(TokenInvalidException $e, $request){
            return  response()->json(['error'=>'Invalid token'],401);
        });
        $this->renderable(function(TokenInvalidException $e, $request){
            return  response()->json(['error'=>'Invalid token'],401);
        });
        $this->renderable(function (TokenExpiredException $e, $request) {
            return response()->json(['error'=>'Token has Expired'],401);
        });

        $this->renderable(function (JWTException $e, $request) {
            return response()->json(['error'=>'Token not parsed'],401);
        });

    }
}
