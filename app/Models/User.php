<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
   /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier() {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims() {
        return [];
    }


    //Một bác sỹ có nhiều bệnh nhân
    public function patients(){
        return $this->belongsToMany(User::class, "relationship", "doctor_id", "user_id");
    }

    public function patientOfManager(){
        return $this->belongsToMany(User::class, "relationship", "manager_id", "user_id");
    }
    public function doctors(){
        return $this->belongsToMany(User::class, "relationship", "user_id", "doctor_id");
    }
    public function managers(){
        return $this->belongsToMany(User::class, "relationship", "user_id", "manager_id");
    }
    public function building(){
        return $this->belongsTo(Building::class, "building_id", "id");
    }
}
