<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Alert extends Model
{
    use HasFactory;
    protected $table = 'alert';
    public function user_create()
    {
        return $this->belongsTo(User::class, "create_by_id", "id");
    }
}
