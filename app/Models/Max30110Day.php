<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Max30110Day extends Model
{
    use HasFactory;
    protected $table = 'max30110_days';
}
