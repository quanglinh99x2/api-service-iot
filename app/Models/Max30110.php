<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon;


class Max30110 extends Model
{
    use HasFactory;
    protected $table = 'max30110';
    public function getCreatedAtAttribute($date)
{
    return Carbon::parse($date)->format('Y-m-d H:i:s');
}
}
