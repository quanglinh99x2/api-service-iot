<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Airdata extends Model
{
    use HasFactory;
    protected $table = 'airdata';
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
